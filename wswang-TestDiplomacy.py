#!/usr/bin/env python3

# ----------------------
# diplomacy/Diplomacy.py
# William S. Wang
# ----------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_print, diplomacy_solve

# -------------
# TestDiplomacy
# -------------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Madrid Hold\n"
        a,c,su = diplomacy_read(s)
        self.assertEqual(a,  "A")
        self.assertEqual(c, "Madrid")
        self.assertEqual(su, "")

    def test_read_2(self):
        s = "C London Support B\n"
        a,c,su = diplomacy_read(s)
        self.assertEqual(a,  "C")
        self.assertEqual(c, "London")
        self.assertEqual(su, "B")

    def test_read_3(self):
        s = "B Barcelona Move Madrid\n"
        a,c,su = diplomacy_read(s)
        self.assertEqual(a,  "B")
        self.assertEqual(c, "Madrid")
        self.assertEqual(su, "")

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        end = {"A":"Madrid"}
        diplomacy_print(w, end)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_print_2(self):
        w = StringIO()
        end = {"A":"[dead]", "B":"Madrid", "C":"London"}
        diplomacy_print(w, end)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_print_3(self):
        w = StringIO()
        end = {"A":"[dead]", "B":"[dead]", "C":"[dead]"}
        diplomacy_print(w, end)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_solve_3(self):
        r = StringIO("A London Move Paris\nB Paris Move London\nC Amsterdam Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Paris\nB London\nC Amsterdam\n")

    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB London Move Madrid\nC Paris Move London\nD Amsterdam Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\nD Amsterdam\n")

    def test_solve_5(self):
        r = StringIO("")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "")

    def test_solve_6(self):
        r = StringIO("A Washington Move Austin\nB Austin Support D\nC London Support A\nD Houston Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

# ----
# main
# ----

if __name__ == "__main__":  #pragma: no cover
    main()
