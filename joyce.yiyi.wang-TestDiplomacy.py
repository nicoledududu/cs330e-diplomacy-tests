#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Joyce Wang
# -------------------------------

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_start, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Madrid Hold\n"
        lst = diplomacy_read(s)
        self.assertListEqual(lst,  ['A', 'Madrid', 'Hold'])

    def test_read_2(self):
        s = "B Barcelona Move Madrid\n"
        lst = diplomacy_read(s)
        self.assertListEqual(lst,  ['B', 'Barcelona', 'Move', 'Madrid'])

    def test_read_3(self):
        s = "C London Support B\n"
        lst = diplomacy_read(s)
        self.assertListEqual(lst,  ['C', 'London', 'Support', 'B'])

    # ----
    # start
    # ----

    def test_start_1(self):
        l = diplomacy_start([['A', 'Madrid', 'Hold']])
        self.assertListEqual(l, [{'army': 'A',
                                  'loc': 'Madrid',
                                  'action': 'Hold',
                                  'supporting': None,
                                  'num_supported': 0,
                                  'died': False}])

    def test_start_2(self):
        l = diplomacy_start([['B', 'Barcelona', 'Move', 'Madrid']])
        self.assertListEqual(l, [{'army': 'B',
                                  'loc': 'Madrid',
                                  'action': 'Move',
                                  'supporting': None,
                                  'num_supported': 0,
                                  'died': False}])

    def test_start_3(self):
        l = diplomacy_start([['C', 'London', 'Support', 'B']])
        self.assertListEqual(l, [{'army': 'C',
                                  'loc': 'London',
                                  'action': 'Support',
                                  'supporting': 'B',
                                  'num_supported': 0,
                                  'died': False}])

    def test_start_4(self):
        l = diplomacy_start([['B', 'Barcelona', 'Move', 'Madrid'],
                             ['C', 'London', 'Support', 'B']])
        self.assertListEqual(l, [{'army': 'B',
                                  'loc': 'Madrid',
                                  'action': 'Move',
                                  'supporting': None,
                                  'num_supported': 1,
                                  'died': False},
                                 {'army': 'C',
                                  'loc': 'London',
                                  'action': 'Support',
                                  'supporting': 'B',
                                  'num_supported': 0,
                                  'died': False}])

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval([{'army': 'A',
                             'loc': 'Madrid',
                             'action': 'Hold',
                             'supporting': None,
                             'num_supported': 0,
                             'died': False}])
        self.assertListEqual(v, ['A Madrid'])

    def test_eval_2(self):
        v = diplomacy_eval([{'army': 'A',
                             'loc': 'Madrid',
                             'action': 'Hold',
                             'supporting': None,
                             'num_supported': 0,
                             'died': False},
                            {'army': 'B',
                             'loc': 'Madrid',
                             'action': 'Move',
                             'supporting': None,
                             'num_supported': 1,
                             'died': False},
                            {'army': 'C',
                             'loc': 'London',
                             'action': 'Support',
                             'supporting': 'B',
                             'num_supported': 0,
                             'died': False}])
        self.assertListEqual(v, ['A [dead]', 'B Madrid', 'C London'])

    def test_eval_3(self):
        v = diplomacy_eval([{'army': 'A',
                             'loc': 'Madrid',
                             'action': 'Hold',
                             'supporting': None,
                             'num_supported': 0,
                             'died': False},
                            {'army': 'B',
                             'loc': 'London',
                             'action': 'Move',
                             'supporting': None,
                             'num_supported': 0,
                             'died': False},
                            {'army': 'C',
                             'loc': 'Austin',
                             'action': 'Move',
                             'supporting': None,
                             'num_supported': 0,
                             'died': False}])
        self.assertListEqual(v, ['A Madrid', 'B London', 'C Austin'])

    def test_eval_4(self):
        v = diplomacy_eval([{'army': 'A',
                             'loc': 'Madrid',
                             'action': 'Hold',
                             'supporting': None,
                             'num_supported': 0,
                             'died': False},
                            {'army': 'B',
                             'loc': 'Madrid',
                             'action': 'Move',
                             'supporting': None,
                             'num_supported': 0,
                             'died': False}])
        self.assertListEqual(v, ['A [dead]', 'B [dead]'])

    def test_eval_5(self):
        v = diplomacy_eval([{'army': 'A',
                             'loc': 'Madrid',
                             'action': 'Support',
                             'supporting': 'B',
                             'num_supported': 1,
                             'died': False},
                            {'army': 'B',
                             'loc': 'Madrid',
                             'action': 'Move',
                             'supporting': 'A',
                             'num_supported': 2,
                             'died': False}])
        self.assertListEqual(v, ['A [dead]', 'B Madrid'])

    def test_eval_6(self):
        v = diplomacy_eval([{'army': 'A',
                             'loc': 'Madrid',
                             'action': 'Support',
                             'supporting': 'A',
                             'num_supported': 2,
                             'died': False},
                            {'army': 'B',
                             'loc': 'Madrid',
                             'action': 'Support',
                             'supporting': 'B',
                             'num_supported': 2,
                             'died': False},
                            {'army': 'C',
                             'loc': 'Madrid',
                             'action': 'Support',
                             'supporting': 'C',
                             'num_supported': 1,
                             'died': False}])
        self.assertListEqual(v, ['A [dead]', 'B [dead]', 'C [dead]'])

    def test_eval_7(self):
        v = diplomacy_eval([{'army': 'A',
                             'loc': 'Madrid',
                             'action': 'Hold',
                             'supporting': None,
                             'num_supported': 2,
                             'died': False},
                            {'army': 'B',
                             'loc': 'Madrid',
                             'action': 'Hold',
                             'supporting': None,
                             'num_supported': 2,
                             'died': False},
                            {'army': 'C',
                             'loc': 'Madrid',
                             'action': 'Hold',
                             'supporting': None,
                             'num_supported': 1,
                             'died': False}])
        self.assertListEqual(v, ['A [dead]', 'B [dead]', 'C [dead]'])

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, ['A Madrid'])
        self.assertEqual(w.getvalue(), 'A Madrid\n')

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, ['A [dead]', 'B Madrid', 'C London'])
        self.assertEqual(w.getvalue(), 'A [dead]\nB Madrid\nC London\n')

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, ['A Madrid', 'B London', 'C Austin'])
        self.assertEqual(w.getvalue(), 'A Madrid\nB London\nC Austin\n')

    def test_print_4(self):
        w = StringIO()
        diplomacy_print(w, ['A [dead]', 'B [dead]'])
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\n')

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO('A Madrid Hold\n')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), 'A Madrid\n')

    def test_solve_2(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), 'A [dead]\nB Madrid\nC London\n')

    def test_solve_3(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\n')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), 'A [dead]\nB [dead]\n')

    def test_solve_4(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD [dead]\n')
        
# ----
# main
# ----


if __name__ == "__main__": #pragma: no cover
    main()
